# FRANKENSTEIN: Estructuras + Haskell + git

El proyecto Frankenstein consiste en una cantidad de tareas, en orden creciente de dificultad, que cubren el contenido de la parte de Haskell de Estructuras de Datos.

El objetivo principal en Frankenstein consiste en “matar los _undefined_”. Un _undefined_ representa un espacio en blanco que se debe completar con código que cumpla la funcionalidad esperada.

El comando `stack test` permite correr los tests de la tarea. Al principio todos ellos deben estar en rojo. La meta es que todos pasen a estar en verde.

## Tarea 02

Completar el cuerpo de las funciones usando pattern matching (sin usar la expresion `if-then-else`)

A partir de ahora, uds deben poner los parametros que toman las funciones.

### NOTAS:

- Los números negativos en Haskell se escriben entre paréntesis (Haskell espera que ¨-" sea un operador binario).
- Muchas de las funciones a implementar ya son parte de Haskell, pero con diferente nombre. Dichas funciones tienen el comentario `{-AKA: <nombre de función de Haskell> -}`

---

Autor: Román García (nykros@gmail.com)
