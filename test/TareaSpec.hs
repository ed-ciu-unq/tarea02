module TareaSpec (spec) where
import Test.Hspec
import Tarea

spec :: Spec
spec =
  describe "Pruebas para la tarea" $ do
    it "negar True" $
      negar True `shouldBe` False

    it "negar False" $
      negar False `shouldBe` True

    it "saludar conocidos" $
      saludar "Juan" `shouldBe` "Hola!"

    it "saludar conocidos" $
      saludar "Pablo" `shouldBe` "Hola!"

    it "saludar desconocido" $
      saludar "Pedro" `shouldBe` "Chau"

    it "primera" $
      primera ("A",4) `shouldBe` "A"

    it "segunda" $
      segunda ("A",4) `shouldBe` 4

    it "sumaPar" $
      sumaPar (23,2) `shouldBe` 25

    it "esVacia de Vacia" $
      esVacia [] `shouldBe` True

    it "esVacia de una lista no vacía" $
      esVacia [1..] `shouldBe` False
  
    it "segundoElem" $
      segundoElem [2, 5, 7, 4] `shouldBe` 5

    it "esListaDeTres False" $
      esListaDeTres [2, 5, 7, 4] `shouldBe` False
    
    it "esListaDeTres True" $
      esListaDeTres [2, 5, 4] `shouldBe` True

    it "esListaDeTresIguales True" $
      esListaDeTresIguales ["A", "A", "A"] `shouldBe` True

    it "esListaDeTresIguales False" $
      esListaDeTresIguales ["A", "A", "A", "B"] `shouldBe` False

    it "esListaDeTresIguales False" $
      esListaDeTresIguales ["A", "A" ] `shouldBe` False

    it "esListaDeTresIguales False" $
      esListaDeTresIguales ["A", "A", "B"] `shouldBe` False

    it "cabezaDePrimera" $
        cabezaDePrimera ([3,7,9,11],True) `shouldBe` 3

    it "segundasIguales True" $
        segundasIguales (2, "Hola") (True, "Hola") `shouldBe` True

    it "segundasIguales False" $
        segundasIguales (2, "Hola") (True, "Chau") `shouldBe` False
   
    it "cosaRara" $
        cosaRara ((2,3), "Hola", "Chau") `shouldBe` "Chau"

    it "cosaRara" $
        cosaRara ((3,2), "Hola", "Chau") `shouldBe` "Hola"
