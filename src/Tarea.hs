module Tarea where

-- Dado un valor booleano, devuelve su contrario. {-AKA: not -}
negar :: Bool -> Bool
negar = undefined


-- Dado un String, si es "Pablo" o "Juan" devuelve "Hola!", sino devuelve "Chau"
saludar :: String -> String
saludar = undefined


-- Dado un par de elementos devuelve el primero. {-AKA: fst -}
primera :: (a, b) -> a
primera = undefined


-- Dado un par de elementos, devuelve el segundo. {-AKA: snd -}
segunda :: (a, b) -> b
segunda = undefined


-- Dado un par de numeros enteros, devuelve la suma de los mismos
sumaPar :: (Int, Int) -> Int
sumaPar = undefined


-- Dado una lista de elementos, devuelve True si esta vacia. {-AKA: null -}
esVacia :: [a] -> Bool
esVacia = undefined


-- EJEMPLO
-- Dada una lista de elementos, devuelve el segundo elemento de la lista.
-- La funcion fallará si la lista tiene menos de dos elementos
segundoElem :: [a] -> a
segundoElem (x:y:xs) = y


-- Devuelve True si la lista tiene tres elementos
esListaDeTres :: [a] -> Bool
esListaDeTres = undefined


-- Devuelve True si la lista tiene tres elementos
esListaDeTresIguales ::Eq a => [a] -> Bool
esListaDeTresIguales = undefined


-- Dado un par que en la primera componente tiene una lista, devolver el 
-- primer elemento de la lista
-- La función no está definida si la lista está vacía
cabezaDePrimera :: ([a],b) -> a
cabezaDePrimera = undefined


-- Dados dos pares, determinar si las segundas componentes de los pares son iguales
segundasIguales :: Eq b =>  (a,b) -> (c,b) -> Bool
segundasIguales = undefined

-- Devuelve el tercer elemento de una lista.
-- Parcial si la lista tiene menos de tres elementos
tercerElem :: [a] -> a
tercerElem (a:b:c:xs) = c


-- Implementar el xor
xor :: Bool -> Bool -> Bool
xor = undefined


-- Dado una tripleta, cuya primera componente es un par de numeros (a,b),
-- Devolver la segunda componente si a >b y la tercera caso contrario
cosaRara :: ((Int,Int),a,a) -> a
cosaRara = undefined